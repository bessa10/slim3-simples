#!/bin/bash

PATH_SCRIPT=$(dirname $0)


mysqladmin --defaults-extra-file=$PATH_SCRIPT/mysql.cnf drop -f tarefas_db
mysqladmin --defaults-extra-file=$PATH_SCRIPT/mysql.cnf create tarefas_db


mysql --defaults-extra-file=$PATH_SCRIPT/mysql.cnf tarefas_db < $PATH_SCRIPT/schemas/db.sql