create table tarefas_tb (
	`id_tarefa` int primary key auto_increment,
	`nome_tarefa` varchar(30),
	`status` int(1) DEFAULT 0,
	`tm_tarefa` timestamp DEFAULT current_timestamp
	);

insert into tarefas_tb (`nome_tarefa`) VALUES ("Primeira tarefa"), ("Mais uma tarefa");
insert into tarefas_tb (`nome_tarefa`, `status`) VALUES ("Continuando a tarefa", 1);