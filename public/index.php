<?php 

require "../vendor/autoload.php";

$config = require "../app/config/config.php";

//vai guardar a instancia do slim
$app = new Slim\App($config);

//carregar os arquivos
require "../app/config/dependencies.php";
require "../app/config/routes.php";

$app->run();