
$("#btn-adicionar").click(function(){
	var tarefa = $("input[name=tarefa]").val();

	//criando requisição ajax com jquery
	$.ajax({
		"method": "POST",
		"data": {"tarefa": tarefa},
		"url": "/tarefas",
		"success": function (tarefas) {
			$(".tarefas").html("");
			$(".tarefas").html(tarefas);
		},
		"error": function (error) {
			console.log(error);
		}
	});
});


//função para deixar sublinhado quando clicar no checkbox
$("input[name=tarefa]").change(function() {
	var verificacao = $(this).prop("checked"), id_tarefa = $(this).val(), status = 0;

	if (verificacao) {
		status = 1;
	}


	$.ajax({
		"method": "PUT",
		"data": {"id_tarefa": id_tarefa, "status": status}, //dados que serão enviados para requisição
		"url": "/tarefas",
		"success": function(){
			window.location.reload();
		} 
	});
});