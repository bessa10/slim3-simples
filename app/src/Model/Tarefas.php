<?php 

namespace Aranda\Model;

class Tarefas
{
	private $db;
	private $logger;

	public function __construct($db, $logger)
	{
		$this->db = $db;
		$this->logger = $logger;
	}

	public function buscarTodos()
	{
		$query = $this->db->createQueryBuilder();

		$query->select("*")->from("tarefas_tb");

		$resultado = $query->execute();

		$resultado_array = $resultado->fetchAll(\PDO::FETCH_ASSOC);

		return $resultado_array;
	}

	public function atualizarTarefa($dados)
	{
		$query = $this->db->createQueryBuilder();

		$query->update("tarefas_tb", "t")
			->set("t.status", "?")
			->where("t.id_tarefa = ?")
			->setParameter(0, $dados['status'])
			->setParameter(1, $dados['id_tarefa'])
			->execute();

		return true;	
	}

	public function inserirTarefa($dados)
	{
		$query = $this->db->createQueryBuilder();

		$query->insert("tarefas_tb")
			->values([
				"nome_tarefa" => "?"
			])
			->setParameter(0, $dados['tarefa'])
			->execute();

		$id_tarefa = $this->db->lastInsertId();
		
		return $id_tarefa;	
	}
}
