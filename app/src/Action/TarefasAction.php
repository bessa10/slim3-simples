<?php

namespace Aranda\Action;

class TarefasAction
{
	private $model;
	private $view;

	public function __construct($model, $view)
	{
		$this->model = $model;
		$this->view = $view;
	}

	public function dispatch($request, $response, $args)
	{
		//pegar os dados do formulário
		$this->model->inserirTarefa($request->getParsedBody());

		$tarefas = $this->model->buscarTodos();

		//vai carrefar uma outra view com os resultados
		$this->view->render($response, 'tarefas.html', ['tarefas' => $tarefas]);

		return $response;
	}
}