<?php

namespace Aranda\Action;

class AtualizarTarefaAction
{
	private $model;

	public function __construct($model)
	{
		$this->model = $model;
	}

	public function dispatch($request, $response, $args)
	{
		//pegar os dados
		$dados = $request->getParsedBody();

		//mandar os dados para o model
		$this->model->atualizarTarefa($dados);

		return json_encode(array("msg" => true));
	}
}