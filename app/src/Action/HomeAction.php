<?php

namespace Aranda\Action;

class HomeAction
{
	private $view;
	private $logger;
	private $tarefaModel;

	public function __construct($view, $logger, $tarefaModel)
	{
		$this->view = $view;
		$this->logger = $logger;
		$this->tarefaModel = $tarefaModel;
	}

	public function dispatch($request, $response, $args)
	{
		$this->logger->addDebug('Feito pedido para a URI /', array('user_agent' => $_SERVER['HTTP_USER_AGENT']));
		
		$tarefas = $this->tarefaModel->buscarTodos();
		$this->view->render($response, 'home.html', ['tarefas' => $tarefas]);

		return $response;
	}
} 