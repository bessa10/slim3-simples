<?php

class TarefasTest extends \PHPUnit_Framework_TestCase
{
	private $model;
	private $db;

	public function __construct()
	{
		$config = require __DIR__ . "/../../../app/config/config.php";
		$app = new Slim\App($config);
		require __DIR__ . "/../../../app/config/dependencies.php";

		$container = $app->getContainer();

		$this->model = $container["Aranda\Model\Tarefas"];
		$this->db = $container["db"];
	}

	public function setUp()
	{
		//executar o script para refazer o banco de dados
		exec(__DIR__ . "/../../../cli/setup-db.sh"); //basta colocar o caminho
	}

	public function testBuscarTodos()
	{
		$resultado = $this->model->buscarTodos();

		$this->assertInternalType("array", $resultado, "Espera que a variável de retorno seja um array");
		$this->assertEquals(3, count($resultado));

		//testando uma linha do resultado
		$this->assertEquals("Primeira tarefa", $resultado[0]['nome_tarefa']);
		$this->assertEquals("0", $resultado[0]['status']);
	}

	public function testAtualizarTarefa()
	{
		//verificar estado atual do bando de dados
		$dados = [
			"id_tarefa" => 1,
			"status" => 1
		];

		$query = $this->db->createQueryBuilder();
		$antes = $query->select("*")
					->from("tarefas_tb")
					->where("status = 0")
					->execute()
					->fetchAll(\PDO::FETCH_ASSOC);

		$this->assertEquals(2, count($antes));
		//fim da verificação de estado atual do banco

		//verificar se realmente atualizou alguma coisa
		$this->model->atualizarTarefa($dados);

		$dados = [
			"id_tarefa" => 1,
			"status" => 1
		];

		$query = $this->db->createQueryBuilder();
		$depois = $query->select("*")
					->from("tarefas_tb")
					->where("status = 0")
					->execute()
					->fetchAll(\PDO::FETCH_ASSOC);		

		$this->assertEquals(1, count($depois));				

	}

	public function testInserirTarefa()
	{
		$dados = [
			"tarefa" => "Teste testando"
		];

		$id_tarefa = $this->model->inserirTarefa($dados);

		$query = $this->db->createQueryBuilder();

		//fazendo a consulta no banco
		$resultado = $query
			->select("*")
			->from("tarefas_tb")
			->where("id_tarefa = ?")
			->setParameter(0, $id_tarefa)
			->execute()
			->fetchAll(\PDO::FETCH_ASSOC); //retorna todas as linhas do resultado e fetch retorna somente uma linha

			$this->assertEquals(1, count($resultado));
			$this->assertEquals("Teste testando", $resultado[0]["nome_tarefa"]);
	}
}