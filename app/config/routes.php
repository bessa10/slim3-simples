<?php

$app->get("/", 'Aranda\Action\HomeAction:dispatch')
	->setName('home');

$app->group('/tarefas', function(){
	$this->map(['POST'], '', 'Aranda\Action\TarefasAction:dispatch');
	$this->map(['PUT'], '', 'Aranda\Action\AtualizarTarefaAction:dispatch');

});	

