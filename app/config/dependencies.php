<?php

$container = $app->getContainer();

//colocar as dependencias
//view
$container['view'] = function ($c) {
	//instanciar e configurar o twig
	$templates = __DIR__ . "/../templates";

	$view = new \Slim\Views\Twig($templates, [
		'cache' => false,
		'debug' => true
	]);

	$view->addExtension(new \Slim\Views\TwigExtension(
		$c['router'],
		$c['request']->getUri()
	));

	return $view;
};

//actions
$container['Aranda\Action\HomeAction'] = function ($c) {
	return new Aranda\Action\HomeAction($c['view'], $c['logger'], $c['Aranda\Model\Tarefas']);
};	

$container['Aranda\Action\AtualizarTarefaAction'] = function ($c) {
	return new Aranda\Action\AtualizarTarefaAction($c['Aranda\Model\Tarefas']);	
};

$container['Aranda\Action\TarefasAction'] = function ($c) {
	return new Aranda\Action\TarefasAction($c['Aranda\Model\Tarefas'], $c['view']);
};

//models
$container['Aranda\Model\Tarefas'] = function ($c) {
	return new Aranda\Model\Tarefas($c['db'], $c['logger']);
};

//monolog
$container['logger'] = function ($c) {
	$logger = new \Monolog\Logger('app');
	//configurar o nome e local onde sera salvo
	$filename = __DIR__ . '/../logs/app.log';

	$stream = new \Monolog\Handler\StreamHandler(
		$filename,
		\Monolog\Logger::DEBUG
		);

	$logger->pushHandler($stream);

	return $logger;
};

//DB
$container['db'] = function ($c) {
	$config = new \Doctrine\DBAL\Configuration();

	return \Doctrine\DBAL\DriverManager::getConnection($c['settings']['db'], $config);
};